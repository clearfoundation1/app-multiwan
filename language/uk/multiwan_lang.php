<?php

$lang['multiwan_app_description'] = 'Додаток «Мульти-WAN» дозволяє підключати вашу систему до кількох Інтернет-з’єднань. Мульти-WAN пропонує багато переваг для середовищ, які вимагають надійного з’єднання, включаючи балансування навантаження, сегрегацію пакетів і автоматичне перемикання при збоях.';
$lang['multiwan_app_name'] = 'Мульти-WAN';
$lang['multiwan_backup'] = 'Резервний';
$lang['multiwan_destination_port_rule'] = 'Надсилання мережевого трафіку для порту через певний WAN';
$lang['multiwan_destination_port_rules'] = 'Правила на основі порту призначення';
$lang['multiwan_in_use'] = 'Використовується';
$lang['multiwan_mode'] = 'Режим';
$lang['multiwan_multiwan'] = 'Мульти-WAN';
$lang['multiwan_multiwan_status'] = 'Статус Мульти-WAN';
$lang['multiwan_network_status'] = 'Стан';
$lang['multiwan_offline'] = 'Офлайн';
$lang['multiwan_online'] = 'Онлайн';
$lang['multiwan_primary'] = 'Головний';
$lang['multiwan_source_based_route'] = 'Система в локальній мережі завжди використовує певний WAN';
$lang['multiwan_source_based_routes'] = 'Маршрути на основі джерела';
$lang['multiwan_standby'] = 'Очікування';
$lang['multiwan_updating_status'] = 'Мульти-WAN оновлює інформацію про стан, це займе лише кілька секунд...';
$lang['multiwan_weight'] = 'Пріоритет';
$lang['multiwan_weight_invalid'] = 'Пріоритет Мульти-WAN недійсний.';
$lang['multiwan_weight_out_of_range'] = 'Пріоритет Мульти-WAN виходить за межі діапазону.';
